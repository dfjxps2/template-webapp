// 详情-共享信息
export default {
  userUrl: '/common/user/loginUser',// 获取登录信息url
  userLogoutUrl: '/common/user/logout',// 退出登录url
  // 存储sessionStorage变量名称
  permissions: 'permissions',
  list: [

    {
      'menuId': '1',
      'name': '首页',
      'icon': 'geren',
      'url': 'home/home'
    },
    {
      'menuId': '2',
      'name': '测试菜单',
      'url': '',
      'permissions': '',
      'icon': 'editor',
      'list': [
        {
          'menuId': '21',
          'name': '子菜单1',
          'icon': 'editor',
          'url': ''
        },
        {
          'menuId': '22',
          'name': '子菜单2',
          'icon': 'editor',
          'url': ''
        },
      ]
    },

  ]
}

